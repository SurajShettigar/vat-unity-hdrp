# Vertex Animation Textures (VAT) for Unity HDRP

This project shows a sample of simulating small-scale fluids, cloth and rigidbodies in Houdini and exporting them as 
Vertex Animation Textures (VATs). These are then played back in real-time engines such as Unity through Shaders.

## Examples

| Cloth | Rigidbody | Fluid |
| --- | --- | --- |
| ![Cloth](./images/cloth_sim.gif) | ![Rigidbody](./images/rigidbody_sim.gif) | ![Fluid](./images/fluid_sim.gif)

## Features

- 3 different simple Houdini simulations.
- Houdini SOP HDA to simplify VAT export.
- Unity HDRP Sample project.
- HDRP Shaders for Cloth, Rigidbody and Fluid simulation VATs.
- Unity Custom Tool to simplify simulation setup.
- Warning messages in Unity when the setup is invalid.

## Screenshots

### Houdini

| HDA | Example Simulation |
| --- | --- |
| ![HDA](./images/houdini_custom_sop_hda.png) | ![Example Simulation](./images/houdini_simulation.png) |


### Unity

| Custom Tool | Validation Warnings |
| --- | --- |
| ![Custom Tool](./images/unity_custom_vat_editor.png) | ![Validation Warnings](./images/unity_custom_editor_error.png) |


## Requirements
- git
- git-lfs
- Unity 2021.3.3f1 and upwards (HDRP project [V.12.1.6])
- <b>Optional:</b> Houdini 19.0.561 with SideFX Labs installed (to modify/create simulations)

## Installation
1. Pull the repository. (This project uses git-lfs, make sure it is installed).
2. The Houdini source file is in `houdini` directory from root.
3. Add the project through Unity Hub <b>or</b> open the project directly in Unity.

## Usage
### Unity Project
- Open the `ShowcaseScene` to view the example simulations. It can be located under `Assets/Scenes/ShowcaseScene`.
- All simulations are present in `Assets/Simulations` folder.
- The tool to validate and initialize simulations can be accessed through toolbar under `VAT --> Simulation Manager`.
![VAT Tool](./images/unity_custom_editor_location.png)
- Make sure the simulation folder in the tool window points to the appropriate directory inside `Assets`. It takes in relative path from `Assets` folder
- It will list all the present simulations in the folder. If it is valid there should be a green checkmark or else a red error. Hovering over it displays the error message.
- Any new simulations needs to be initialized by clicking on the `Initialize` button next to its name.
- Prefabs can be created and Instantiated by clicking on their respective buttons.

### VAT Shaders
- All the shaders are present in the `Assets/Shaders` folder.
- All the shaders are done in Shader Graphs and subgraphs.
- The shader closely follows Houdini's own shaders that comes with SideFX Labs. The shaders have been simplified and organized for easier understanding.

### Houdini Project
- The Houdini source file is in `houdini` directory from root.
- The project has three example simulations. 
- The VATs are exported through a custom `VAT Exporter` HDA.
- The HDA settings are quite-straigtforward with options to select texture size, type of simulation, name and export folder.
- When it comes to Fluid simulation, the export is divided into <b>two passes</b>. This can be done by selecting its dropdown, and exporting it twice for two different passes.
- <b>NOTE: Texture Width may need to be increased if the export fails for certain simulations with higher point count.</b>







