using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Linq;
using UnityEngine;

public enum SimulationType
{
    None,
    Cloth,
    Fluid,
    Rigidbody
}

public class SimulationData
{
    public string name = "Simulation";
    public SimulationType type = SimulationType.None;
    public string folderPath = null;
    public string meshPath = null;
    public string positionTexturePath = null;
    public string rotationTexturePath = null;
    public string colorTexturePath = null;
    public string lookupTableTexturePath = null;
    public string materialPath = null;
    public string prefabPath = null;

    public bool isValid(out string message)
    {
        if(type == SimulationType.None)
        {
            message = "Simulation Type not set. Make sure the folder has appopritate Simulation type suffix. One of _cloth, _fluid, _rigidbody";
            return false;
        }

        if (meshPath == null || meshPath.Length == 0)
        {
            message = "No Valid Mesh file found. Make sure the mesh file is of FBX extension and has a _mesh suffix.";
            return false;
        }
        if (positionTexturePath == null || positionTexturePath.Length == 0)
        {
            message = "No Valid Position texture found.  Make sure the texture has _pos suffix.";
            return false;
        }

        switch (type)
        {
            case SimulationType.Cloth:
                break;
            case SimulationType.Fluid:
                if (rotationTexturePath == null || rotationTexturePath.Length == 0)
                {
                    message = "No Valid Rotation texture found. Rotation texture is necessary for Fluid simulations. Make sure the texture has _rot suffix.";
                    return false;
                }
                if (lookupTableTexturePath == null || lookupTableTexturePath.Length == 0)
                {
                    message = "No Valid Lookup Table texture found. Lookup Table is necessary for Fluid simulations. Make sure the texture has _lookup suffix.";
                    return false;
                }
                break;
            case SimulationType.Rigidbody:
                if (rotationTexturePath == null || rotationTexturePath.Length == 0)
                {
                    message = "No Valid Rotation texture found. Rotation texture is necessary for Rigidbody simulations. Make sure the texture has _rot suffix.";
                    return false;
                }
                break;
        }
        message = "Simulation Data is Valid";
        return true;
    }

    public static SimulationData GetSimulationDataFromFolder(string folderPath)
    {
        SimulationData data = new SimulationData();
        if (Directory.Exists(folderPath))
        {
            data.folderPath = folderPath;
            
            string folderName = Path.GetFileName(folderPath);

            string simTypeName = folderName.Split("_").Last();

            data.name = folderName.Replace($"_{simTypeName}", "");

            SimulationType simType = SimulationType.Cloth;
            Enum.TryParse<SimulationType>(simTypeName, true, out simType);
            data.type = simType;

            foreach (string p in Directory.EnumerateFiles(folderPath, "*.*", SearchOption.AllDirectories))
            {
                string fileName = Path.GetFileName(p);
                if(fileName.EndsWith("meta"))
                    continue;

                if (fileName.Contains("mesh"))
                {
                    data.meshPath = p;
                }
                else if (fileName.Contains("pos"))
                {
                    data.positionTexturePath = p;
                }
                else if (fileName.Contains("rot"))
                {
                    data.rotationTexturePath = p;
                }
                else if (fileName.Contains("col"))
                {
                    data.colorTexturePath = p;
                }
                else if (fileName.Contains("lookup"))
                {
                    data.lookupTableTexturePath = p;
                }
                else if(p.ToLower() == GetMaterialPath(data.folderPath, data.name).ToLower())
                {
                    data.materialPath = GetMaterialPath(data.folderPath, data.name);
                }
                else if(p.ToLower() == GetPrefabPath(data.folderPath, data.name).ToLower())
                {
                    data.prefabPath = GetPrefabPath(data.folderPath, data.name);
                }
            }
        }
        return data;
    }

    public static string GetMaterialPath(string folderPath, string name)
    {
        return Path.Join(folderPath, name + "_mat.mat");
    }

    public static string GetPrefabPath(string folderPath, string name)
    {
        return Path.Join(folderPath, name + ".prefab");
    }
}
