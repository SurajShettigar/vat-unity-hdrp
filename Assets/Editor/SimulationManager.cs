using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using UnityEditor.Presets;

public class SimulationManager : EditorWindow
{
    private const string ICON_CHECK_FILE = "check.png";
    private const string ICON_ERROR_FILE = "error.png";

    private const string PRESET_MESH_FILE = "VAT_Mesh_Preset.preset";
    private const string PRESET_TEXTURE_FILE = "VAT_Texture_Preset.preset";
    private const string PRESET_MATERIAL_CLOTH_FILE = "VAT_Material_Cloth_Preset.preset";
    private const string PRESET_MATERIAL_FLUID_FILE = "VAT_Material_Fluid_Preset.preset";
    private const string PRESET_MATERIAL_RIGIDBODY_FILE = "VAT_Material_Rigidbody_Preset.preset";

    private const string SHADER_CLOTH = "Shader Graphs/Cloth";
    private const string SHADER_FLUID = "Shader Graphs/Fluid";
    private const string SHADER_RIGIDBODY = "Shader Graphs/Rigidbody";

    private const string SHADER_PROP_POS_TEX = "_Position_Texture";
    private const string SHADER_PROP_ROT_TEX = "_Rotation_Texture";
    private const string SHADER_PROP_LOOKUP_TEX = "_Lookup_Table";
    private const string SHADER_PROP_COLOR_TEX = "_Color_Texture";

    private GUIStyle STYLE_SECTION_HEADING;

    private Texture ICON_CHECK;
    private Texture ICON_ERROR;

    private Preset PRESET_MODEL;
    private Preset PRESET_TEXTURE;

    private Preset PRESET_MATERIAL_CLOTH;
    private Preset PRESET_MATERIAL_FLUID;
    private Preset PRESET_MATERIAL_RIGIDBODY;


    private string _simFolderPath = "/Simulations";
    private List<SimulationData> _currentSimData = new List<SimulationData>();

    [MenuItem("VAT/Simulation Manager")]
    private static void ShowWindow()
    {
        SimulationManager window = GetWindow<SimulationManager>();
        window.titleContent = new GUIContent("Simulation Manager");
        window.Show();
    }

    private void OnEnable()
    {
        STYLE_SECTION_HEADING = new GUIStyle
        {
            fontSize = 16,
            fontStyle = FontStyle.Bold,
            normal = new GUIStyleState
            {
                textColor = Color.white
            }
        };

        ICON_CHECK = (Texture)EditorGUIUtility.Load(ICON_CHECK_FILE);
        ICON_ERROR = (Texture)EditorGUIUtility.Load(ICON_ERROR_FILE);

        PRESET_MODEL = (Preset)EditorGUIUtility.Load(PRESET_MESH_FILE);
        PRESET_TEXTURE = (Preset)EditorGUIUtility.Load(PRESET_TEXTURE_FILE);

        PRESET_MATERIAL_CLOTH = (Preset)EditorGUIUtility.Load(PRESET_MATERIAL_CLOTH_FILE);
        PRESET_MATERIAL_FLUID = (Preset)EditorGUIUtility.Load(PRESET_MATERIAL_FLUID_FILE);
        PRESET_MATERIAL_RIGIDBODY = (Preset)EditorGUIUtility.Load(PRESET_MATERIAL_RIGIDBODY_FILE);

        PopulateSimulationData();
    }

    private void OnGUI()
    {
        FolderSelector();
        ListSimulations();
    }

    private void FolderSelector()
    {
        EditorGUILayout.LabelField("Settings", STYLE_SECTION_HEADING);
        EditorGUILayout.BeginHorizontal();
        _simFolderPath = EditorGUILayout.TextField("Simulations Folder", _simFolderPath, GUILayout.MaxWidth(1024));
        bool isClicked = GUILayout.Button("...", EditorStyles.miniButtonRight, GUILayout.Width(32));
        if (isClicked)
        {
            string path = EditorUtility.OpenFolderPanel("Select Simualtion Folder", _simFolderPath, "");
            if (path.Length > 0)
            {
                _simFolderPath = GetAssetRelativePath(path);
                PopulateSimulationData();
            }
        }
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(16);
    }

    private void ListSimulations()
    {
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField("Simulations", STYLE_SECTION_HEADING);

        bool refreshList = GUILayout.Button("Refresh", EditorStyles.miniButtonRight, GUILayout.Width(64));
        if (refreshList)
            PopulateSimulationData();

        EditorGUILayout.EndHorizontal();

        EditorGUI.indentLevel = 0;
        EditorGUILayout.BeginVertical();
        EditorGUI.indentLevel = 1;

        if (_currentSimData.Count > 0)
        {
            foreach (SimulationData s in _currentSimData)
            {
                EditorGUILayout.BeginHorizontal();
                bool isSimValid = s.isValid(out string message);
                GUILayout.Button(new GUIContent(isSimValid ? ICON_CHECK : ICON_ERROR, message),
                    new GUIStyle { padding = new RectOffset(0, 0, 0, 0) },
                    GUILayout.MaxWidth(16), GUILayout.MaxHeight(16));
                EditorGUILayout.LabelField(s.name, EditorStyles.boldLabel, GUILayout.MaxWidth(256));
                if (isSimValid)
                {
                    EditorGUILayout.EnumPopup(s.type, GUILayout.MaxWidth(128));
                    bool isInitialize = GUILayout.Button("Initialize", GUILayout.MaxWidth(128));
                    if (isInitialize)
                    {
                        InitializeSim(s);
                    }

                    if (s.materialPath?.Length > 0)
                    {
                        bool createPrefab = GUILayout.Button("Create Prefab", GUILayout.MaxWidth(128));
                        if(createPrefab)
                            CreatePrefab(s);
                    }
                    if (s.prefabPath?.Length > 0)
                    {
                        bool instantiate = GUILayout.Button("Instantiate", GUILayout.MaxWidth(128));
                        if(instantiate)
                            InstantiatePrefab(s);
                    }
                }
                EditorGUILayout.EndHorizontal();
                GUILayout.Space(8);
            }
        }
        else
        {
            EditorGUILayout.LabelField("Empty", EditorStyles.largeLabel);
        }

        EditorGUILayout.EndVertical();
        GUILayout.Space(16);
        EditorGUI.indentLevel = 0;
    }

    private void PopulateSimulationData()
    {
        string rootFolder = GetAbsolutePath(_simFolderPath);
        _currentSimData = new List<SimulationData>();
        if (Directory.Exists(rootFolder))
        {
            foreach (string s in Directory.EnumerateDirectories(rootFolder))
            {
                _currentSimData.Add(SimulationData.GetSimulationDataFromFolder(s));
            }
        }
    }

    private void InitializeSim(SimulationData sim)
    {
        if (sim.meshPath?.Length > 0)
        {
            AssetImporter asset = AssetImporter.GetAtPath(GetProjectRelativePath(sim.meshPath));
            PRESET_MODEL.ApplyTo(asset);
            asset.SaveAndReimport();
        }

        if (sim.positionTexturePath?.Length > 0)
        {
            AssetImporter asset = AssetImporter.GetAtPath(GetProjectRelativePath(sim.positionTexturePath));
            PRESET_TEXTURE.ApplyTo(asset);
            asset.SaveAndReimport();
        }

        if (sim.rotationTexturePath?.Length > 0)
        {
            AssetImporter asset = AssetImporter.GetAtPath(GetProjectRelativePath(sim.rotationTexturePath));
            PRESET_TEXTURE.ApplyTo(asset);
            asset.SaveAndReimport();
        }

        if (sim.lookupTableTexturePath?.Length > 0)
        {
            AssetImporter asset = AssetImporter.GetAtPath(GetProjectRelativePath(sim.lookupTableTexturePath));
            PRESET_TEXTURE.ApplyTo(asset);
            asset.SaveAndReimport();
        }

        if (sim.colorTexturePath?.Length > 0)
        {
            AssetImporter asset = AssetImporter.GetAtPath(GetProjectRelativePath(sim.colorTexturePath));
            PRESET_TEXTURE.ApplyTo(asset);
            asset.SaveAndReimport();
        }

        Material material;
        sim.materialPath = SimulationData.GetMaterialPath(sim.folderPath, sim.name);
        material = AssetDatabase.LoadAssetAtPath<Material>(GetProjectRelativePath(sim.materialPath));
        if (material == null)
        {
            switch (sim.type)
            {
                case SimulationType.Cloth:
                    material = new Material(Shader.Find(SHADER_CLOTH));
                    break;
                case SimulationType.Fluid:
                    material = new Material(Shader.Find(SHADER_FLUID));
                    break;
                case SimulationType.Rigidbody:
                    material = new Material(Shader.Find(SHADER_RIGIDBODY));
                    break;
                default:
                    material = new Material(Shader.Find(SHADER_CLOTH));
                    break;
            }
            AssetDatabase.CreateAsset(material, GetProjectRelativePath(sim.materialPath));

            if (sim.materialPath?.Length > 0)
            {
                switch (sim.type)
                {
                    case SimulationType.Cloth:
                        PRESET_MATERIAL_CLOTH.ApplyTo(material);
                        break;
                    case SimulationType.Fluid:
                        PRESET_MATERIAL_FLUID.ApplyTo(material);
                        break;
                    case SimulationType.Rigidbody:
                        PRESET_MATERIAL_RIGIDBODY.ApplyTo(material);
                        break;
                }
                if (sim.positionTexturePath?.Length > 0)
                {
                    Texture tex = AssetDatabase.LoadAssetAtPath<Texture>(GetProjectRelativePath(sim.positionTexturePath));
                    material.SetTexture(SHADER_PROP_POS_TEX, tex);
                }
                if (sim.rotationTexturePath?.Length > 0)
                {
                    Texture tex = AssetDatabase.LoadAssetAtPath<Texture>(GetProjectRelativePath(sim.rotationTexturePath));
                    material.SetTexture(SHADER_PROP_ROT_TEX, tex);
                }
                if (sim.lookupTableTexturePath?.Length > 0)
                {
                    Texture tex = AssetDatabase.LoadAssetAtPath<Texture>(GetProjectRelativePath(sim.lookupTableTexturePath));
                    material.SetTexture(SHADER_PROP_LOOKUP_TEX, tex);
                }
                if (sim.colorTexturePath?.Length > 0)
                {
                    Texture tex = AssetDatabase.LoadAssetAtPath<Texture>(GetProjectRelativePath(sim.colorTexturePath));
                    material.SetTexture(SHADER_PROP_COLOR_TEX, tex);
                }
            }
        }
        else
            Debug.Log($"Found existing material for {sim.name}. Using the same.");

    }

    private void CreatePrefab(SimulationData sim)
    {
        sim.prefabPath = SimulationData.GetPrefabPath(sim.folderPath, sim.name);
        GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(GetProjectRelativePath(sim.prefabPath));
        if (prefab == null)
        {
            prefab = new GameObject(sim.name);
            GameObject simAsset = AssetDatabase.LoadAssetAtPath<GameObject>(GetProjectRelativePath(sim.meshPath));
            GameObject simObject = (GameObject)PrefabUtility.InstantiatePrefab(simAsset, prefab.transform);
            MeshRenderer simRenderer = simObject.GetComponentInChildren<MeshRenderer>();
            if (simRenderer != null)
                simRenderer.material = AssetDatabase.LoadAssetAtPath<Material>(GetProjectRelativePath(sim.materialPath));

            PrefabUtility.SaveAsPrefabAssetAndConnect(prefab, GetProjectRelativePath(sim.prefabPath), InteractionMode.AutomatedAction, out bool isPrefabSaved);
            if (isPrefabSaved)
            {
                Debug.Log($"{sim.name} saved as prefab in directory: {GetProjectRelativePath(sim.prefabPath)}");
            }
            else
            {
                Debug.Log($"{sim.name} failed to saved as prefab");
            }
        }
        else
            Debug.Log($"Found existing prefab for {sim.name}. Using the same.");
    }

    private void InstantiatePrefab(SimulationData sim)
    {
        GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(GetProjectRelativePath(sim.prefabPath));
        PrefabUtility.InstantiatePrefab(prefab);
    }

    private string GetProjectRelativePath(string path)
    {
        return "Assets/" + GetAssetRelativePath(path);
    }

    private string GetAssetRelativePath(string path)
    {
        return path.Replace(Application.dataPath, "");
    }

    private string GetAbsolutePath(string path)
    {
        if (!path.Contains(Application.dataPath))
        {
            return Path.Join(Application.dataPath, path);
        }
        else
            return path;
    }
}
