//UNITY_SHADER_NO_UPGRADE
#ifndef VATUTILS_INCLUDED
#define VATUTILS_INCLUDED

void DecodeQuaternion_float(float3 Rotation, float MaxComponent, out float4 Quaternion)
{
    float w = sqrt(1.0 - pow(Rotation.x, 2) - pow(Rotation.y, 2) - pow(Rotation.z, 2));
    float4 q = float4(0, 0, 0, 1);

    switch(MaxComponent)
    {
        case 0:
            q = float4(Rotation.x, Rotation.y, Rotation.z, w);
            break;
        case 1:
            q = float4(w, Rotation.y, Rotation.z, Rotation.x);
            break;
        case 2:
            q = float4(Rotation.x, -w, Rotation.z, -Rotation.y);
            break;
        case 3:
            q = float4(Rotation.x, Rotation.y, -w, -Rotation.z);
            break;
        default:
            q = float4(Rotation.x, Rotation.y, Rotation.z, w);
            break;
    }
    Quaternion = q;
}

void RotateVector_float(float3 InVector, float4 Rotation, out float3 OutVector)
{
    OutVector = InVector.xyz + 2.0 * cross(Rotation.xyz, 
                                     cross(Rotation.xyz, 
                                           InVector.xyz)
                                        + Rotation.w*InVector.xyz);
}
#endif